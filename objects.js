let studentsArr = [];
studentsArr[0] = [];
studentsArr[1] = [];
studentsArr[2] = [];
studentsArr[3] = [];

studentsArr[0] = {
    firstName: "Jean",
    lastName: "Reno",
    age: 26,
    subjects: {
        javascript: 62,
        react: 57,
        python: 88,
        java: 90,
        credits: [4, 7, 6, 3]
    }
}

studentsArr[1] = {
    firstName: "Claude",
    lastName: "Monet",
    age: 19,
    subjects: {
        javascript: 77,
        react: 52,
        python: 92,
        java: 67,
        credits: [4, 7, 6, 3]
    }
}

studentsArr[2] = {
    firstName: "Van",
    lastName: "Gogh",
    age: 21,
    subjects: {
        javascript: 51,
        react: 98,
        python: 65,
        java: 70,
        credits: [4, 7, 6, 3]
    }
}
studentsArr[3] = {
    firstName: "Dam",
    lastName: "Square",
    age: 36,
    subjects: {
        javascript: 82,
        react: 53,
        python: 80,
        java: 65,
        credits: [4, 7, 6, 3]
    }
}

/************************************/

console.log("The sum of the scores:");

studentsArr[0].sum =
  studentsArr[0].subjects.javascript +
  studentsArr[0].subjects.react +
  studentsArr[0].subjects.python +
  studentsArr[0].subjects.java;
console.log(
  `    ${studentsArr[0].firstName} ${studentsArr[0].lastName}    --->  ${studentsArr[0].sum}`
);

studentsArr[1].sum =
  studentsArr[1].subjects.javascript +
  studentsArr[1].subjects.react +
  studentsArr[1].subjects.python +
  studentsArr[1].subjects.java;
console.log(
  `    ${studentsArr[1].firstName} ${studentsArr[1].lastName} --->  ${studentsArr[1].sum}`
);

studentsArr[2].sum =
  studentsArr[2].subjects.javascript +
  studentsArr[2].subjects.react +
  studentsArr[2].subjects.python +
  studentsArr[2].subjects.java;
console.log(
  `    ${studentsArr[2].firstName} ${studentsArr[2].lastName}     --->  ${studentsArr[2].sum}`
);

studentsArr[3].sum =
  studentsArr[3].subjects.javascript +
  studentsArr[3].subjects.react +
  studentsArr[3].subjects.python +
  studentsArr[3].subjects.java;
console.log(
  `    ${studentsArr[3].firstName} ${studentsArr[3].lastName}   --->  ${studentsArr[3].sum}`
);

// /******************************************/

console.log("");
console.log("The average of the scores:");

studentsArr[0].average =
  (studentsArr[0].subjects.javascript *
    studentsArr[0].subjects.credits[0] +
    studentsArr[0].subjects.react *
      studentsArr[0].subjects.credits[1] +
    studentsArr[0].subjects.python *
      studentsArr[0].subjects.credits[2] +
    studentsArr[0].subjects.java *
      studentsArr[0].subjects.credits[3]) /
  (studentsArr[0].subjects.credits[0] +
    studentsArr[0].subjects.credits[1] +
    studentsArr[0].subjects.credits[2] +
    studentsArr[0].subjects.credits[3]);
console.log(
  `    ${studentsArr[0].firstName} ${studentsArr[0].lastName}    --->  ${studentsArr[0].average}`
);

studentsArr[1].average =
  (studentsArr[1].subjects.javascript *
    studentsArr[1].subjects.credits[0] +
    studentsArr[1].subjects.react *
      studentsArr[1].subjects.credits[1] +
    studentsArr[1].subjects.python *
      studentsArr[1].subjects.credits[2] +
    studentsArr[1].subjects.java *
      studentsArr[1].subjects.credits[3]) /
  (studentsArr[1].subjects.credits[0] +
    studentsArr[1].subjects.credits[1] +
    studentsArr[1].subjects.credits[2] +
    studentsArr[1].subjects.credits[3]);
console.log(
  `    ${studentsArr[1].firstName} ${studentsArr[1].lastName} --->  ${studentsArr[1].average}`
);

studentsArr[2].average =
  (studentsArr[2].subjects.javascript *
    studentsArr[2].subjects.credits[0] +
    studentsArr[2].subjects.react *
      studentsArr[2].subjects.credits[1] +
    studentsArr[2].subjects.python *
      studentsArr[2].subjects.credits[2] +
    studentsArr[2].subjects.java *
      studentsArr[2].subjects.credits[3]) /
  (studentsArr[2].subjects.credits[0] +
    studentsArr[2].subjects.credits[1] +
    studentsArr[2].subjects.credits[2] +
    studentsArr[2].subjects.credits[3]);
console.log(
  `    ${studentsArr[2].firstName} ${studentsArr[2].lastName}     --->  ${studentsArr[2].average}`
);

studentsArr[3].average =
  (studentsArr[3].subjects.javascript *
    studentsArr[3].subjects.credits[0] +
    studentsArr[3].subjects.react *
      studentsArr[3].subjects.credits[1] +
    studentsArr[3].subjects.python *
      studentsArr[3].subjects.credits[2] +
    studentsArr[3].subjects.java *
      studentsArr[3].subjects.credits[3]) /
  (studentsArr[3].subjects.credits[0] +
    studentsArr[3].subjects.credits[1] +
    studentsArr[3].subjects.credits[2] +
    studentsArr[3].subjects.credits[3]);
console.log(
  `    ${studentsArr[3].firstName} ${studentsArr[3].lastName}   --->  ${studentsArr[3].average}`
);

// // /**************************************/

console.log("");
console.log("GPA:");

// if(studentsArr[0].average >= 91){
//     studentsArr[0].gpa = 4;
// }
// else if(studentsArr[0].average >= 81){
//     studentsArr[0].gpa = 3;
// }
// else if(studentsArr[0].average >= 71){
//     studentsArr[0].gpa = 2;
// }

// else if(studentsArr[0].average >= 61){
//     studentsArr[0].gpa = 1;
// }
// else if(studentsArr[0].average >= 51){
//     studentsArr[0].gpa = 0.5;
// }

studentsArr[0].gpa =
  studentsArr[0].average >= 91
    ? 4
    : studentsArr[0].average >= 81
    ? 3
    : studentsArr[0].average >= 71
    ? 2
    : studentsArr[0].average >= 61
    ? 1
    : 0.5;

console.log(
  `    ${studentsArr[0].firstName} ${studentsArr[0].lastName}    --->  ${studentsArr[0].gpa}`
);

// if(studentsArr[1].average >= 91){
//     studentsArr[1].gpa = 4;
// }
// else if(studentsArr[1].average >= 81){
//     studentsArr[1].gpa = 3;
// }
// else if(studentsArr[1].average >= 71){
//     studentsArr[1].gpa = 2;
// }

// else if(studentsArr[1].average >= 61){
//     studentsArr[1].gpa = 1;
// }
// else if(studentsArr[1].average >= 51){
//     studentsArr[1].gpa = 0.5;
// }

studentsArr[1].gpa =
  studentsArr[1].average >= 91
    ? 4
    : studentsArr[1].average >= 81
    ? 3
    : studentsArr[1].average >= 71
    ? 2
    : studentsArr[1].average >= 61
    ? 1
    : 0.5;

console.log(
  `    ${studentsArr[1].firstName} ${studentsArr[1].lastName} --->  ${studentsArr[1].gpa}`
);

// if(studentsArr[2].average >= 91){
//     studentsArr[2].gpa = 4;
// }
// else if(studentsArr[2].average >= 81){
//     studentsArr[2].gpa = 3;
// }
// else if(studentsArr[2].average >= 71){
//     studentsArr[2].gpa = 2;
// }

// else if(studentsArr[2].average >= 61){
//     studentsArr[2].gpa = 1;
// }
// else if(studentsArr[2].average >= 51){
//     studentsArr[2].gpa = 2.5;
// }

studentsArr[2].gpa =
  studentsArr[2].average >= 91
    ? 4
    : studentsArr[2].average >= 81
    ? 3
    : studentsArr[2].average >= 71
    ? 2
    : studentsArr[2].average >= 61
    ? 1
    : 0.5;

console.log(
  `    ${studentsArr[2].firstName} ${studentsArr[2].lastName}     --->  ${studentsArr[2].gpa}`
);

// if(studentsArr[3].average >= 91){
//     studentsArr[3].gpa = 4;
// }
// else if(studentsArr[3].average >= 81){
//     studentsArr[3].gpa = 3;
// }
// else if(studentsArr[3].average >= 71){
//     studentsArr[3].gpa = 2;
// }

// else if(studentsArr[3].average >= 61){
//     studentsArr[3].gpa = 1;
// }
// else if(studentsArr[3].average >= 51){
//     studentsArr[3].gpa = 3.5;
// }

studentsArr[3].gpa =
  studentsArr[3].average >= 91
    ? 4
    : studentsArr[3].average >= 81
    ? 3
    : studentsArr[3].average >= 71
    ? 2
    : studentsArr[3].average >= 61
    ? 1
    : 0.5;

console.log(
  `    ${studentsArr[3].firstName} ${studentsArr[3].lastName}   --->  ${studentsArr[3].gpa}`
);

// /********************************************************/

console.log("");
console.log("Awards Ceremony:");

let avgScore =
  (studentsArr[0].average +
    studentsArr[1].average +
    studentsArr[2].average +
    studentsArr[3].average) /
  4;

console.log(
  studentsArr[0].average >= avgScore
    ? `    Congratulations, ${studentsArr[0].firstName}!!! You've got a red diploma.`
    : `    Opps :( ${studentsArr[0].firstName}, try harder next time.`
);

console.log(
  studentsArr[1].average >= avgScore
    ? `    Congratulations, ${studentsArr[1].firstName}!!! You've got a red diploma.`
    : `    Opps :( ${studentsArr[1].firstName}, try harder next time.`
);

console.log(
  studentsArr[2].average >= avgScore
    ? `    Congratulations, ${studentsArr[2].firstName}!!! You've got a red diploma.`
    : `    Opps :( ${studentsArr[2].firstName}, try harder next time.`
);

console.log(
  studentsArr[3].average >= avgScore
    ? `    Congratulations, ${studentsArr[3].firstName}!!! You've got a red diploma.`
    : `    Opps :( ${studentsArr[3].firstName}, try harder next time.`
);

// /*******************************************************************/

let highestGpa = [];

highestGpa = studentsArr[0];

console.log("");
console.log("The students who have the highest GPA:");
console.log("");

if (highestGpa.gpa < studentsArr[1].gpa) {
  highestGpa = studentsArr[1];
}
if (highestGpa.gpa < studentsArr[2].gpa) {
  highestGpa = studentsArr[2];
}
if (highestGpa.gpa < studentsArr[3].gpa) {
  highestGpa = studentsArr[3];
}

console.log("First Name     Last Name     GPA");

if (highestGpa.gpa == studentsArr[0].gpa) {
  console.log(
    `  ${studentsArr[0].firstName}           ${studentsArr[0].lastName}         ${studentsArr[0].gpa}`
  );
}
if (highestGpa.gpa == studentsArr[1].gpa) {
  console.log(
    `  ${studentsArr[1].firstName}         ${studentsArr[1].lastName}        ${studentsArr[1].gpa}`
  );
}
if (highestGpa.gpa == studentsArr[2].gpa) {
  console.log(
    `  ${studentsArr[2].firstName}            ${studentsArr[2].lastName}         ${studentsArr[2].gpa}`
  );
}
if (highestGpa.gpa == studentsArr[3].gpa) {
  console.log(
    `  ${studentsArr[3].firstName}            ${studentsArr[3].lastName}       ${studentsArr[3].gpa}`
  );
}

// /***********************************************/

let bestAdult = [];

bestAdult = studentsArr[0];

console.log("");
console.log("The best student above 21:");
console.log("");

if (
  studentsArr[1].age > 21 &&
  bestAdult.average < studentsArr[1].average
) {
  bestAdult = studentsArr[1];
}
if (
  studentsArr[2].age > 21 &&
  bestAdult.average < studentsArr[2].average
) {
  bestAdult = studentsArr[2];
}
if (
  studentsArr[3].age > 21 &&
  bestAdult.average < studentsArr[3].average
) {
  bestAdult = studentsArr[3];
}

console.log("First Name     Last Name     Age     Average Score");
console.log(
  `  ${bestAdult.firstName}           ${bestAdult.lastName}        ${bestAdult.age}          ${bestAdult.average}`
);

// /************************************************/

console.log("");
console.log("The best front-end student:");
console.log("");

let bestFrontEnd = [];

bestFrontEnd = studentsArr[0];
bestFrontEnd.score =
  (studentsArr[0].subjects.javascript +
    studentsArr[0].subjects.react) /
  2;

if (
  bestFrontEnd.score <
  (studentsArr[1].subjects.javascript +
    studentsArr[1].subjects.react) /
    2
) {
  bestFrontEnd = studentsArr[1];
  bestFrontEnd.score =
    (studentsArr[1].subjects.javascript +
      studentsArr[1].subjects.react) /
    2;
}
if (
  bestFrontEnd.score <
  (studentsArr[2].subjects.javascript +
    studentsArr[2].subjects.react) /
    2
) {
  bestFrontEnd = studentsArr[2];
  bestFrontEnd.score =
    (studentsArr[2].subjects.javascript +
      studentsArr[2].subjects.react) /
    2;
}
if (
  bestFrontEnd.score <
  (studentsArr[3].subjects.javascript +
    studentsArr[3].subjects.react) /
    2
) {
  bestFrontEnd = studentsArr[3];
  bestFrontEnd.score =
    (studentsArr[3].subjects.javascript +
      studentsArr[3].subjects.react) /
    2;
}

console.log("First Name     Last Name     Score (Js + React)");
console.log(
  `  ${bestFrontEnd.firstName}            ${bestFrontEnd.lastName}               ${bestFrontEnd.score}`
);
